#include "fsm.hpp"

#include <iostream>

using namespace fsm;

// State

void State::turn_off() const {
    std::cout << "ACTION: turn_off" << std::endl;
}

void State::turn_middle() const {
    std::cout << "ACTION: turn_middle" << std::endl;
}

void State::turn_full() const {
    std::cout << "ACTION: turn_full" << std::endl;
}

void State::invalid() const {
    std::cout << "ACTION: invalid" << std::endl;
}

// Full

std::unique_ptr<State> Full::off() const {
    turn_middle();
    return std::make_unique<MidPlugged>();
}

std::unique_ptr<State> Full::on() const {
    return std::make_unique<Full>();
}

std::unique_ptr<State> Full::plug() const {
    invalid();
    return std::make_unique<Invalid>();
}

std::unique_ptr<State> Full::unplug() const {
    turn_middle();
    return std::make_unique<WaitingToFull>();
}

const std::string Full::to_string() const {
    return "Full";
}

// Invalid

std::unique_ptr<State> Invalid::off() const {
    invalid();
    return std::make_unique<Invalid>();
}

std::unique_ptr<State> Invalid::on() const {
    invalid();
    return std::make_unique<Invalid>();
}

std::unique_ptr<State> Invalid::plug() const {
    invalid();
    return std::make_unique<Invalid>();
}

std::unique_ptr<State> Invalid::unplug() const {
    invalid();
    return std::make_unique<Invalid>();
}

const std::string Invalid::to_string() const {
    return "Invalid";
}

// MidPlugged

std::unique_ptr<State> MidPlugged::off() const {
    turn_off();
    return std::make_unique<OffPlugged>();
}

std::unique_ptr<State> MidPlugged::on() const {
    turn_full();
    return std::make_unique<Full>();
}

std::unique_ptr<State> MidPlugged::plug() const {
    invalid();
    return std::make_unique<Invalid>();
}

std::unique_ptr<State> MidPlugged::unplug() const {
    return std::make_unique<MidUnplugged>();
}

const std::string MidPlugged::to_string() const {
    return "Mid plugged";
}

// MidUnplugged

std::unique_ptr<State> MidUnplugged::off() const {
    turn_off();
    return std::make_unique<OffUnplugged>();
}

std::unique_ptr<State> MidUnplugged::on() const {
    return std::make_unique<WaitingToFull>();
}

std::unique_ptr<State> MidUnplugged::plug() const {
    return std::make_unique<MidPlugged>();
}

std::unique_ptr<State> MidUnplugged::unplug() const {
    invalid();
    return std::make_unique<Invalid>();
}

const std::string MidUnplugged::to_string() const {
    return "Mid unplugged";
}

// OffPlugged

std::unique_ptr<State> OffPlugged::off() const {
    return std::make_unique<OffPlugged>();
}

std::unique_ptr<State> OffPlugged::on() const {
    turn_middle();
    return std::make_unique<MidPlugged>();
}

std::unique_ptr<State> OffPlugged::plug() const {
    invalid();
    return std::make_unique<Invalid>();
}

std::unique_ptr<State> OffPlugged::unplug() const {
    return std::make_unique<OffUnplugged>();
}

const std::string OffPlugged::to_string() const {
    return "Off plugged";
}

// OffUnplugged

std::unique_ptr<State> OffUnplugged::off() const {
    return std::make_unique<OffUnplugged>();
}

std::unique_ptr<State> OffUnplugged::on() const {
    turn_middle();
    return std::make_unique<MidUnplugged>();
}

std::unique_ptr<State> OffUnplugged::plug() const {
    return std::make_unique<OffPlugged>();
}

std::unique_ptr<State> OffUnplugged::unplug() const {
    invalid();
    return std::make_unique<Invalid>();
}

const std::string OffUnplugged::to_string() const {
    return "Off unplugged";
}

// WaitingToFull

std::unique_ptr<State> WaitingToFull::off() const {
    turn_middle();
    return std::make_unique<MidUnplugged>();
}

std::unique_ptr<State> WaitingToFull::on() const {
    return std::make_unique<WaitingToFull>();
}

std::unique_ptr<State> WaitingToFull::plug() const {
    turn_full();
    return std::make_unique<Full>();
}

std::unique_ptr<State> WaitingToFull::unplug() const {
    return std::make_unique<Invalid>();
}

const std::string WaitingToFull::to_string() const {
    return "Waiting to full";
}

// Lamp

Lamp::Lamp() :
    m_state {std::make_unique<OffUnplugged>()} {
}

void Lamp::off() {
    update(m_state->off());
}

void Lamp::on() {
    update(m_state->on());
}

void Lamp::plug() {
    update(m_state->plug());
}

void Lamp::unplug() {
    update(m_state->unplug());
}

void Lamp::update(std::unique_ptr<State> state) {
    std::cout << "Transition from: " + m_state->to_string() +
                 " to: " + state->to_string() << std::endl;
    m_state = std::move(state);
}
