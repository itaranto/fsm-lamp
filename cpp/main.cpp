#include <iostream>

#include "fsm.hpp"

enum Event : int {
    OFF,
    ON,
    PLUG,
    UNPLUG
};

void dispatch_event(const Event event, fsm::Lamp& lamp) {
    switch(event) {
    case (OFF):
        lamp.off();
        break;
    case (ON):
        lamp.on();
        break;
    case (PLUG):
        lamp.plug();
        break;
    case (UNPLUG):
        lamp.unplug();
        break;
    default:
        std::cout << "Invalid event, please try again" << std::endl;
        break;
    }
}

int main() {
    fsm::Lamp lamp;

    std::string line;
    while (std::getline(std::cin, line)) {
        try {
            const int event = std::stoi(line);
            dispatch_event(static_cast<Event>(event), lamp);
        } catch (const std::invalid_argument&) {
            std::cout << "Cannot convert to integer, please try again" << std::endl;
        } catch (const std::out_of_range&) {
            std::cout << "Integer out of range, please try again" << std::endl;
        }
    }

    return 0;
}
