use std::io;

mod fsm;

fn main() {
    let mut lamp = fsm::Lamp::new();
    let mut eof = false;

    while !eof {
        let mut event = String::new();

        eof = match io::stdin().read_line(&mut event) {
            Ok(n) => n == 0,
            Err(error) => {
                panic!("Failed to read line. error: {}", error);
            }
        };

        match event.trim().parse() {
            Ok(0) => {
                lamp.off();
            }
            Ok(1) => {
                lamp.on();
            }
            Ok(2) => {
                lamp.plug();
            }
            Ok(3) => {
                lamp.unplug();
            }
            Ok(_) | Err(_) => {
                println!("Please enter a valid event number");
            }
        };
    }
}
