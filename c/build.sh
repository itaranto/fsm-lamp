#!/bin/sh
set -e

build_dir=$(dirname "$0")/build

mkdir -p "$build_dir"
cd "$build_dir"
cmake ..
make "-j$(nproc)"
